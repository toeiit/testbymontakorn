-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 14, 2022 at 05:40 PM
-- Server version: 8.0.16
-- PHP Version: 8.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test`
--

-- --------------------------------------------------------

--
-- Table structure for table `person`
--

CREATE TABLE `person` (
  `idPerson` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `Identitycard` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `firstname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `lastname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `gender` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `brithdate` date NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `occupation` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `phonenumber` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `IsAdmin` int(13) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `person`
--

INSERT INTO `person` (`idPerson`, `Identitycard`, `username`, `firstname`, `lastname`, `gender`, `brithdate`, `email`, `occupation`, `address`, `phonenumber`, `password`, `IsAdmin`) VALUES
('0db33717-88f6-11ec-ab21-54ab3ae8fb63', '1577246136359', 'admin', 'แอดมิน', 'ทดสอบ', '0', '0000-00-00', 'admin@gmail.com', 'โปรแกรมเมอร์', 'รพ.สวนดอก', '053214586', '1234', 1),
('eed4dbac-8d9b-11ec-a0d1-54ab3ae8fb63', '1547859965874', 'dekdee', 'เด็กดี', 'ทดสอบ', '0', '2013-06-06', 'dekdee@gmail.com', 'ครู', '12/5 หมู่ 5 อ.เมือง ต.ศรีภูมิ จ.เชียงใหม่ 50220', '0958745263', '12345', 0),
('f2527f4a-8d9a-11ec-a0d1-54ab3ae8fb63', '1505211145874', 'Maneee', 'มณี', 'ทดสอบ', '0', '1984-03-01', 'Maneee@gmail.com', 'พยาบาล', '25/5  ต.สันกลาง อ.สันกำแพง จ.เชียงใหม่ 50220', '0958745632', '12345', 0),
('23080dd8-8d9c-11ec-a0d1-54ab3ae8fb63', '1525244458745', 'montree', 'มนตรี', 'ทดสอบ', '0', '1972-02-08', 'montree@gmail.com', 'หมอ', '55 หมู่ 5 อ.เมือง ต.ศรีภูมิ จ.เชียงใหม่ 50220', '0958745856', '12345', 0),
('dac46f22-8d9c-11ec-a0d1-54ab3ae8fb63', '1525478596582', 'pongsak', 'พงค์ศักดิ์', 'ทดสอบ', '0', '1997-02-01', 'pongsak@gmail.com', 'ทนาย', '8/9 หมู่ 8 อ.ดอยสะเก็ด ต.แม่คือ จ.เชียงใหม่ 50100', '0958745869', '12547', 0),
('7371d257-8d9a-11ec-a0d1-54ab3ae8fb63', '150521555856', 'SomMai', 'สมหมาย', 'ทดสอบ', '0', '1998-02-10', 'SomMai@gmail.com', 'พ่อค้า ', '14/5 หมู่ 5 อ.ดอยสะเก็ด ต.แม่คือ จ.เชียงใหม่ 50220', '0954785658', '1234', 0),
('61d37d47-8d9c-11ec-a0d1-54ab3ae8fb63', '1502522254253', 'sompong', 'สมพงษ์', 'ทดสอบ', '0', '1954-02-03', 'sompong@gmail.com', 'พ่อครัว ', '25/9 หมู่ 8 อ.ดอยสะเก็ด ต.แม่คือ จ.เชียงใหม่ 50100', '0958745856', '1234', 0),
('ed729541-8d99-11ec-a0d1-54ab3ae8fb63', '1505245554785', 'somying', 'สมหญิง', 'ทดสอบ', '0', '1982-02-03', 'SomYing@gmsil.com', 'ครู ', '23/7 หมู่ 5 อ.เมือง ต.นิมมาน จ.เชียงใหม่ 50100', '0958745856', '12345', 0),
('a8f9db3d-8d9c-11ec-a0d1-54ab3ae8fb63', '1505211147859', 'Suddee', 'สุดดี', 'ทดสอบ', '0', '1951-02-07', 'Suddee@gmail.com', 'หมอ', '99 หมู่ 8 อ.ดอยสะเก็ด ต.แม่คือ จ.เชียงใหม่ 50100', '0958745869', '123456', 0),
('acdc10c6-8cf8-11ec-91ee-54ab3ae8fb63', '1505212225452', 'toeiit', 'Montakorn', 'konnak', '0', '2022-02-01', 'Montakorntoei@gmail.com', 'ครู', 'ซอย', '0952057695', '1234', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `person`
--
ALTER TABLE `person`
  ADD PRIMARY KEY (`username`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
