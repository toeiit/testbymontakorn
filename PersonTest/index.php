<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" href="stylsheet.css">
</head>

<body>


    <!-- Search form -->

    <div class="container-fluid">
        <div class="row">
            <!-- สร้างแบบฟอร์มด้วย Bootstrap 4-->
            <div class="col-md-4">
                <div class="container">
                    <form name="search_user" id="search_user" method="POST" action="index.php">
                        <div class="form-group row">
                            <p><label for="firstname" class="col-sm-3 col-form-label">ค้นหา</label></p>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="firstname" name="firstname">
                            </div>
                        </div>

                        <div class="form-group row">
                            <!-- <label for="province" class="col-sm-3 col-form-label">&nbsp;</label> -->
                            <div class="col-sm-6">
                                <input type="submit" class="btn btn-primary" id="submit" name="submit" value="ค้นหา">
                            </div>

                            <div class="col-sm-6">
                                <input type="button" class="btn btn-primary" id="resetform" name="resetform"
                                    value="ยกเลิก">
                            </div>
                            <!-- <label for="province"
                                class="col-sm-3 col-form-label">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label> -->
                            <h2><a class="underlineHover" href="Register.php">Register</a></h2>
                            <div class="col-sm-6">
                                <a h></a>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
            <!-- สร้างตารางด้วย Bootstrap 4-->
            <div class="col-md-8">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr class="bg-primary text-light">
                            <th>NO</th>
                            <th class="text-center">firstname</th>
                            <th class="text-center">lastname</th>
                            <th class="text-center">email</th>
                            <th class="text-center">phonenumber</th>
                            <th class="text-center">แก้ไข</th>
                            <th class="text-center">ลบ</th>


                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <!-- การส่งข้อมูลด้วย jQuery AJAX เพื่อค้นหา ไปที่ไฟล์ search_result.php-->
    <script>
    $(document).ready(function() {
        $.fn.SelectUserData('');
    });

    $(function() {
        // ============================================================================
        // เริ่มต้นให้โหลดข้อมูลทั้งหมดออกมาแสดง โดยเรียกฟังก์ชัน all_users()
        all_users();
        // สร้างฟังก์ชันดึงข้อมูลจากตาราง user ทั้งหมด โดยอ่านจากไฟล์ all_users.php
        function all_users() {
            $.ajax({
                url: 'all_users.php',
                type: 'GET',
                dataType: 'json',
                success: function(data) {
                    // กำหนดตัวแปรเก็บโครงสร้างแถวของตาราง
                    var trstring = "";
                    // ตัวแปรนับจำนวนแถว
                    var countrow = 1;
                    var clickon = "DeleteUser";
                    // วนลูปข้อมูล JSON ลงตาราง
                    $.each(data, function(key, value) {
                        // ทดสอบแสดงชื่อ
                        // console.log(value.fullname);
                        // แสดงค่าลงในตาราง
                        clickon = "DeleteUser" + countrow;
                        console.log(clickon);
                        trstring += `

                                    <tr>
                                        <td class="text-center">${countrow}</td>
                                        <td class="text-center">${value.firstname}</td>
                                        <td class="text-center">${value.lastname}</td>
                                        <td class="text-center">${value.email}</td>
                                        <td class="text-center">${value.phonenumber}</td>
                                        <td class="text-center">
                                            <button type="button" data-toggle="modal" data-target="#exampleModal" data-user-id="EditUser${countrow}" class="btn btn-sm btn-warning" value="$.fn.SelectrowDelete('${value.idPerson}')" onclick="${value.idPerson}">แก้ไข</button>
                                        </td>
                                        <td class="text-center">
                                            <button data-user-id="DeleteUser${countrow}" type="button" class="btn btn-sm btn-danger btn-delete" value="${value.idPerson}" onclick="$.fn.SelectrowDelete('${value.idPerson}')">ลบ</button>
                                        </td>
                                       
                                    </tr>`;

                        $('table tbody').html(trstring);
                        countrow++;
                    });
                }
            });
        }
        // ============================================================================
        // เมื่อมีการ submit form
        $('form#search_user').submit(function(event) {
            event.preventDefault();
            // รับค่าจากฟอร์ม
            $.fn.SelectUserData('');
        });
    });

    $.fn.SelectrowDelete = function(IDPerson) {
        var idPerson = IDPerson;
        console.log(idPerson);
        // $.ajax({
        //             url: 'search_result.php',
        //             type: 'POST',
        //             dataType: 'json',
        //             data: {
        //                 idPerson: idPerson,
        //             },
        //         }
        $.fn.SelectUserData(idPerson);
    }
    $.fn.SelectUserData = function(inputdata) {
        var firstname = $('#firstname').val();
        var idperson = inputdata;
        // ส่งค่าไป search_result.php ด้วย jQuery Ajax
        $.ajax({
            url: 'search_result.php',
            type: 'POST',
            dataType: 'json',
            data: {
                firstname: firstname,
                idperson: idperson,
            },
            cache: false,
            success: function(data) {
                var dataresult = [...data];
                if (dataresult) {
                    // กรณีมีข้อมูล
                    // กำหนดตัวแปรเก็บโครงสร้างแถวของตาราง
                    var trstring = "";
                    // ตัวแปรนับจำนวนแถว
                    var countrow = 1;

                    // วนลูปข้อมูล JSON ลงตาราง 
                    for (let value of dataresult) {
                        clickon = "DeleteUser" + countrow;
                        console.log(clickon);
                        trstring += `
                                    <tr>
                                        <td class="text-center">${countrow}</td>
                                        <td class="text-center">${value.firstname}</td>
                                        <td class="text-center">${value.lastname}</td>
                                        <td class="text-center">${value.email}</td>
                                        <td class="text-center">${value.phonenumber}</td>
                                        <td class="text-center">
                                            <button type="button" data-toggle="modal" data-target="#exampleModal" data-user-id="EditUser${countrow}" class="btn btn-sm btn-warning" value="'${value.idPerson}'" onclick="$.fn.SelectrowDelete(${value.idPerson})">แก้ไข</button>
                                        </td>
                                        <td class="text-center">
                                            <button data-user-id="DeleteUser${countrow}" type="button" class="btn btn-sm btn-danger btn-delete" value="${value.idPerson}" onclick="$.fn.SelectrowDelete('${value.idPerson}')">ลบ</button>
                                        </td>
                                        
                                
                                    </tr>`;
                        $('table tbody').html(trstring);
                        countrow++;
                    }
                } else {
                    // alert('ค้นหาสำเร็จ');
                }
                // alert('ไม่พบข้อมูลที่ค้นหา');
            },
            error: function(xhr, status, error) {
                alert('กรุณากรอกข้อมูลก่อนกดค้นหา');
            },
        });
    }
    // ============================================================================
    // เมื่อกดปุ่มล้างข้อมูลการค้นหา
    $('input#resetform').click(function() {
        // ล้างค่าในฟอร์มทั้งหมด
        $("#search_user").trigger('reset');
        // โฟกัสช่องชื่อ
        $('input#fullname').focus();
        // เรียกแสดงผลข้อมูลทั้งหมด
        all_users();
    });
    </script>
</body>

</html>