<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Register</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>

<body>
    <div style="margin: auto;width: 60%;">
        <div class="alert alert-success alert-dismissible" id="success" style="display:none;">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
        </div>
        <form id="fupForm" name="form1" method="post">
            <div class="form-group">
                <label for="firstname">ชื่อจริง:</label>
                <input type="text" class="form-control" id="firstname" placeholder="ชื่อจริง" name="firstname">
            </div>
            <div class="form-group">
                <label for="lastname">นามสกุล:</label>
                <input type="text" class="form-control" id="lastname" placeholder="นามสกุล" name="lastname">
            </div>
            <div class="form-group">
                <label for="gender">เพศ:</label>
                <input class="form-check-input" type="radio" name="gender" id="gender" value="0">
                <label class="form-check-label" for="female">หญิง</label>
                <input class="form-check-input" type="radio" name="gender" id="gender" value="1">
                <label class="form-check-label" for="man">ชาย</label>
            </div>
            <div class="form-group">
                <label for="email">วันเกิด:</label>
                <input type="date" class="form-control" id="brithdate" placeholder="" name="brithdate">
            </div>
            <div class="form-group">
                <label for="Identitycard">เลขบัตรประชาชน:</label>
                <input type="text" class="form-control" id="Identitycard" placeholder="x-xxxx-xxxxx-xx-x"
                    name="Identitycard">
            </div>
            <div class="form-group">
                <label for="address">ที่อยู่:</label>
                <input type="textarea" class="form-control" id="address" placeholder="ที่อยู่" name="address">
            </div>
            <div class="form-group">
                <label for="occupation">อาชีพ:</label>
                <input type="text" class="form-control" id="occupation" placeholder="อาชีพ" name="occupation">
            </div>
            <div class="form-group">
                <label for="email">Email:</label>
                <input type="email" class="form-control" id="email" placeholder="Email" name="email">
            </div>
            <div class="form-group">
                <label for="phonenumber">เบอร์โทร:</label>
                <input type="text" class="form-control" id="phonenumber" placeholder="xxx-xxxxxxx" name="phonenumber">
            </div>
            <div class="form-group">
                <label for="username">ชื่อผู้ใช้:</label>
                <input type="text" class="form-control" id="username" placeholder="username" name="username">
            </div>
            <div class="form-group">
                <label for="pwd">รหัสผ่าน:</label>
                <input type="password" class="form-control" id="password" placeholder="รหัสผ่าน" name="password">
            </div>
            <div class="form-group">
                <label for="pwd2">ยืนยันรหัสผ่าน:</label>
                <input type="password" class="form-control" id="password_log2" placeholder="ยืนยันรหัสผ่าน"
                    name="password2">
            </div>

            <input type="button" name="save" class="btn btn-primary" value="Save to database" id="butsave">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a class="underlineHover" href="index.php">Home pang</a>
        </form>

    </div>

    <script>
    $(document).ready(function() {
        $('#butsave').on('click', function() {
            $("#butsave").attr("disabled", "disabled");
            var firstname = $('#firstname').val();
            var lastname = $('#lastname').val();
            var gender = $('#gender').val();
            var brithdate = $('#brithdate').val();
            var Identitycard = $('#Identitycard').val();
            var phonenumber = $('#phonenumber').val();
            var email = $('#email').val();
            var occupation = $('#occupation').val();
            var address = $('#address').val();
            var username = $('#username').val();
            var password = $('#password').val();

            if (firstname != "" && lastname != "" && gender != "" && brithdate != "" && Identitycard !=
                "" &&
                phonenumber != "" && email != "" && gender != "" && occupation != "" && address != "" &&
                username != "" && password != ""
            ) {
                $.ajax({
                    url: "save.php",
                    type: "POST",
                    data: {
                        type: 1,
                        firstname: firstname,
                        lastname: lastname,
                        gender: gender,
                        brithdate: brithdate,
                        Identitycard: Identitycard,
                        phonenumber: phonenumber,
                        email: email,
                        occupation: occupation,
                        address: address,
                        username: username,
                        password: password
                    },
                    cache: false,
                    success: function(dataResult) {
                        //var c = JSON.parse(dataResult)
                        alert(dataResult);

                    }
                });
            } else {
                alert('Please fill all the field !');
            }
        });
    });
    </script>
</body>

</html>